using System;
using System.Text.RegularExpressions;
using GenderMaster;
using Xunit;

namespace GenderMasterTests
{
    public class GenderRuleTest
    {
        [Fact]
        public void CreateNewGenderRule()
        {
            var pattern = @"\S*ung$";
            Regex rgxPattern = new Regex(pattern, RegexOptions.IgnoreCase);
            var rule = new GenderRule("-ung/Feminine/die", rgxPattern);

            Assert.Equal("-ung/Feminine/die", rule.Description);
            Assert.Equal(@"\S*ung$", rule.Pattern.ToString());
        }
        
        [Theory]
        [InlineData("Wohnung")]
        [InlineData("WohnUng")]
        public void CheckPositiveRuleAppling(string value)
        {
            var pattern = @"\S*ung$";
            Regex rgxPattern = new Regex(pattern, RegexOptions.IgnoreCase);
            var rule = new GenderRule("-ung/Feminine/die", rgxPattern);

            Assert.True(rule.Check(value));
        }
        
        [Theory]
        [InlineData("Bruder")]
        [InlineData("BruDer")]
        public void CheckNonPositiveRuleAppling(string value)
        {
            var pattern = @"\S*ung$";
            Regex rgxPattern = new Regex(pattern, RegexOptions.IgnoreCase);
            var rule = new GenderRule("-ung/Feminine/die", rgxPattern);

            Assert.False(rule.Check(value));
        }
    }
}