using System.Text.RegularExpressions;

namespace GenderMaster
{
    public class GenderRule
    {
        public string Description { get; }
        public Regex Pattern { get; }

        public GenderRule(string description, Regex pattern)
        {
            Description = description;
            Pattern = pattern;
        }

        public bool Check(string word)
        {
            return Pattern.IsMatch(word);
        }
    }
}